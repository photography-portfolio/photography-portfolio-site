# 📸 Photography Portfolio Website ([www.omkarkalgude.in](https://www.omkarkalgude.in/))

<br/>

<a href="https://www.omkarkalgude.in/">
<img src="https://gitlab.com/photography-portfolio/photography-portfolio-site/-/raw/master/public/assets/img/gallery/cover/BlackLogo.png" width="200">
</a>
<br/>
<br/>

---

##### ✒️ The website is created in collaboration with @Hemanginik and @hedaukartik2009. <br/>

##### ✒️ We have used `ReactJS`, `bootstrap` and `firebase` as our technology stack. <br/>

##### ✒️ The website shows perfect `responsive` behaviour on wide range of devices. <br/>

##### ✒️ Features like `lazy loading` of images have have remarkably increased the performance of website. <br/>

##### ✒️ We have followed due agile process during implementation of the website.<br/>

<br/>

---

###### 💡 Below screenshot shows the performance metrics using [Lighthouse](https://developers.google.com/web/tools/lighthouse)

<a href="https://www.omkarkalgude.in/">
<img src="https://gitlab.com/photography-portfolio/photography-portfolio-site/-/raw/master/performance.png" width="700">
</a>

---

> ❤️ **Special thanks to [Omkar Kalgude](http://www.instagram.com/omkarkalgude_photography/) for believing in us and presenting the** **opportunity to create [www.omkarkalgude.in](https://www.omkarkalgude.in/)**. 🙂
> </br>

---

<img src="https://firebase.google.com/downloads/brand-guidelines/PNG/logo-built_white.png" width="75" alt="firebase logo">
<img src="https://www.logo.wine/a/logo/Bootstrap_(front-end_framework)/Bootstrap_(front-end_framework)-Logo.wine.svg" alt="bootstrap logo" width="75" >
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/220px-React-icon.svg.png" alt="react logo" width="75" >

##### Reach out to me at : &nbsp;&nbsp; <a href="https://www.linkedin.com/in/mayuresh-kakade-ba4658125/"><img src="https://logodix.com/logo/91004.png" alt="linkedin logo" width="25"></a> &nbsp;&nbsp; <a href="https://mail.google.com/mail/?view=cm&fs=1&to=mckakade@outlook.com"><img src="https://logodix.com/logo/4406.png" alt="gmail logo" width="25"></a>
