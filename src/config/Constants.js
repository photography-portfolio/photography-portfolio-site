export const SEND_MAIL_URL =
  "https://us-central1-photography-web-app.cloudfunctions.net/sendMail";

export const SEND_SMS_URL =
  "https://us-central1-photography-web-app.cloudfunctions.net/sendSms";
