const functions = require("firebase-functions");
const admin = require("firebase-admin");
const nodemailer = require("nodemailer");
const twilio = require("twilio");
const constants = require("./NodeConstants");
const cors = require("cors")({ origin: true });
admin.initializeApp();

/**
 * Here we're using Gmail to send
 */
let transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  secure: true,
  auth: {
    user: constants.ADMIN_EMAIL,
    pass: constants.ADMIN_PASS,
  },
});

exports.sendMail = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    const message = req.body.userData.message;
    const email = req.body.userData.email;
    const mobile = req.body.userData.mobile;
    const name = req.body.userData.name;
    const subject = req.body.userData.subject;

    const mailOptions = {
      from: constants.ADMIN_EMAIL, // Something like: Jane Doe <janedoe@gmail.com>
      to: constants.ADMIN_EMAIL,
      subject: constants.WEBSITE_NAME + "---" + subject, // email subject  "From"+name+"<br>"+message,
      html: `<label>From ${name},</label><br><p>${message}</p><br><p>Email: ${email}<br>Phone: ${mobile}</p>`, // email content in HTML
    };

    // returning result
    return transporter.sendMail(mailOptions, (erro, info) => {
      if (erro) {
        return res.send(erro.toString());
      }
      return res.send("Sended");
    });
  });
});
