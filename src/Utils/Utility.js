export const showTitle = (title) => {
  const nameList = title.split("-");
  return nameList.map((item) => item.trim());
};
