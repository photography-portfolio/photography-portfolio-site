import React from "react";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import PropTypes from "prop-types";
import "../Shared/styles/lightbox.css";

const LighBoxModule = (props) => {
  const {
    imagesForLightBox,
    photoIndex,
    onCloseRequest,
    onMovePrevRequest,
    onMoveNextRequest,
  } = props;
  return (
    <Lightbox
      mainSrc={imagesForLightBox[photoIndex].src}
      nextSrc={
        imagesForLightBox[(photoIndex + 1) % imagesForLightBox.length].src
      }
      prevSrc={
        imagesForLightBox[
          (photoIndex + imagesForLightBox.length - 1) % imagesForLightBox.length
        ].src
      }
      onCloseRequest={onCloseRequest}
      onMovePrevRequest={onMovePrevRequest}
      onMoveNextRequest={onMoveNextRequest}
    />
  );
};

LighBoxModule.propTypes = {
  imagesForLightBox: PropTypes.array.isRequired,
  photoIndex: PropTypes.number.isRequired,
  onCloseRequest: PropTypes.func.isRequired,
  onMoveNextRequest: PropTypes.func.isRequired,
  onMovePrevRequest: PropTypes.func.isRequired,
};

export default LighBoxModule;
